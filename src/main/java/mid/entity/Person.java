package mid.entity;

import javax.persistence.*;

@Entity
@Table (name = "midterm")
public class Person {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String firstName;
    private String lastName;
    private String city;
    private String phone;
    private String telegram;
}
